GNU/Linux auto installation files.

## Install

Automatic installation is done by setting a command in installation menu.

#### CentOS / Fedora

```
inst.ks=https://uaik.github.io/os.id.txt
```

#### Debian / Ubuntu

```
url=https://uaik.github.io/os.id.txt
```

## Files

- Debian
  - [Debian VM](debian.vm.txt)
  - [Debian Server](debian.01.txt)
- Red Hat
  - [CentOS Server](centos.01.txt)
  - [Fedora Server](fedora.01.txt)
  - [Fedora Workstation](fedora.02.txt)
  - [Fedora Workstation MINI](fedora.03.txt)
  - [Fedora Workstation MINI EFI](fedora.04.txt)
  - [Oracle Server](oracle.01.txt)

## Authors

- [Yu Dunaev](https://dunaev.dev)
